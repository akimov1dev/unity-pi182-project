﻿using Photon.Pun;
using UnityEngine;


public class Gun : MonoBehaviourPun, IGun
{
    private GameObject _spear;
    private Transform _playerTransform;
    private PlayerPoints _playerPoints;
    private float _nextTimeToFire = 0.0f;

    public Gun(GameObject spear, Transform playerTransform, PlayerPoints playerPoints)
    {
        _spear = spear;
        _playerTransform = playerTransform;
        _playerPoints = playerPoints;
    }

    public void Shoot(float fireRate)
    {

        if (_spear != null && Time.time > _nextTimeToFire)
        {
            _nextTimeToFire = Time.time + 1.0f / fireRate;

            GameObject spear = PhotonNetwork.Instantiate(_spear.name, _playerTransform.position + _playerTransform.forward * 2.5f, _playerTransform.rotation);
            spear.GetComponent<Arrow>().SetPlayer(_playerPoints);
        }
    }

}