﻿using Photon.Pun;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviourPun
{

    [Header("Other")]
    [SerializeField] private TextMeshProUGUI _textNickname = null;
    [SerializeField] private Transform _anchor = null;
    private Camera _gameCamera = null;

    [Header("Rotation")]
    [SerializeField] private float _speedRotateCoefficient = 10f;
    [SerializeField] private float _maxSpeedRotate = 5f;

    [Header("Moving")]
    [SerializeField] private float _moveSpeed = 15f;
    [SerializeField] private LayerMask _layerMaskPlanet;

    [Header("Shooting")]
    [SerializeField] private GameObject _spear = null;
    [SerializeField] private float _fireRate = 1f;

    private IPlayerMovement _playerMovement;
    private IGun _gun;

    private bool _isNotFoundComponent;

    private MeshRenderer _meshRenderer;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        if (!photonView.IsMine) return;

        if (_anchor == null)
        {
            Debug.LogError("Где якорь, Лебовски?");
            _isNotFoundComponent = true;
            return;
        }

        _playerMovement = new PlayerMovement(transform, _anchor);
        _gun = new Gun(_spear, transform, GetComponent<PlayerPoints>());
    }

    private void Start()
    {

        if (!photonView.IsMine) return;

        _gameCamera = Camera.main;
        _gameCamera.GetComponent<GunCamera>().PlayerTransform = transform;

        if (_gameCamera == null)
        {
            Debug.LogError("Где камера, Лебовски?");
            _isNotFoundComponent = true;
            return;
        }

        _textNickname.text = PhotonNetwork.NickName;
        photonView.RPC("SetPlayerNickname", RpcTarget.OthersBuffered, PhotonNetwork.NickName);
    }


    private void Update()
    {

        if (!_isNotFoundComponent && photonView.IsMine)
        {
            Vector3 mouseWorldPos = GetMouseWorldPosition(Input.mousePosition);
            _playerMovement.RotatePlayerAroundPlanet(mouseWorldPos, _speedRotateCoefficient, _maxSpeedRotate);
            _playerMovement.MovePlayerToSelectedPlanet(mouseWorldPos, Input.GetMouseButtonDown(1), _moveSpeed, _layerMaskPlanet);

            if (Input.GetMouseButtonDown(0)) _gun.Shoot(_fireRate);
        }
    }

    private Vector3 GetMouseWorldPosition(Vector2 mousePosition)
    {
        float cameraToPlayerDistance = _gameCamera.transform.position.y - transform.position.y;
        Vector3 mouseWorldPos = _gameCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, cameraToPlayerDistance));
        return mouseWorldPos;
    }

    public void SetStartPlanet(GameObject startPlanet)
    {
        startPlanet.GetComponent<PlanetInfo>().SetIsBusyValue(true);
        _playerMovement.SetCurrentPlanet(startPlanet);
    }

    public void SetColor(Color color)
    {
        _meshRenderer.material.color = color;
        photonView.RPC("SetPlayerColor", RpcTarget.OthersBuffered, color.r, color.g, color.b);
    }

    [PunRPC]
    private void SetPlayerColor(float r, float g, float b)
    {
        _meshRenderer.material.color = new Color(r, g, b);
    }

    [PunRPC]
    private void SetPlayerNickname(string nickname)
    {
        _textNickname.text = nickname;
    }

}
