﻿using UnityEngine;

interface IPlayerMovement
{
    /// <summary>
    /// Перемещение игрока на выбранную планету.
    /// </summary>
    void MovePlayerToSelectedPlanet(Vector3 mouseWorldPos, bool inputGetButtonDown, float moveSpeed, LayerMask layerMask);

    /// <summary>
    /// Вращение игрока вокруг текущей планеты.
    /// </summary>
    void RotatePlayerAroundPlanet(Vector3 mouseWorldPos, float speedRotCoefficient, float maxSpeedRot);

    /// <summary>
    /// Задает текущую планету.
    /// </summary>
    void SetCurrentPlanet(GameObject planet);
}
