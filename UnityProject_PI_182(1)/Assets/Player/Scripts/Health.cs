﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviourPun, IPunObservable
{
    [SerializeField] private int _health = 5;
    [SerializeField] private int _maxHealth = 5;
    [SerializeField] private Slider _healthSlider;

    public int CurrentHealth { get; private set; }

    private void Update()
    {
        CurrentHealth = _health;
        _healthSlider.value = (float)_health / _maxHealth;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(_health);
        }
        else
        {
            _health = (int)stream.ReceiveNext();
        }
    }

    public void TakeDamage(int amount)
    {

        if (photonView.IsMine)
        {
            _health -= amount;
            if (_health <= 0f)
            {
                Die();
            }
        }
    }

    void Die()
    {
        PhotonNetwork.LeaveRoom();
    }
}
