﻿using Photon.Pun;

public class PlayerPoints : MonoBehaviourPun, IPunObservable
{
    public int PointCount { get; private set; }

    public string NickName { get; private set; }

    private void Start()
    {
        PointCount = 0;
        NickName = PhotonNetwork.NickName;
    }

    public void AddPointCount(int pointCount)
    {
        PointCount += pointCount;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(PointCount);
            stream.SendNext(NickName);
        }
        else
        {
           PointCount = (int)stream.ReceiveNext();
           NickName = (string)stream.ReceiveNext();
        }
    }
}
