﻿using Photon.Pun;
using UnityEngine;

public class LaserAim : MonoBehaviourPun
{

    [SerializeField] private float _length = 15f;

    private LineRenderer _lineRenderer;

    private void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        if (!GetComponent<PhotonView>().IsMine) _lineRenderer.enabled = false;
    }

    private void LateUpdate()
    {
        _lineRenderer.SetPosition(0, transform.position + transform.forward * 2.5f);
        _lineRenderer.SetPosition(1, transform.position + transform.forward * _length);
    }
}
