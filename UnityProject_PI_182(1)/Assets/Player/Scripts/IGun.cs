﻿using System.Collections;
using UnityEngine;
public interface IGun
{
    void Shoot(float fireRate);
    //bool GetPlanetToTeleport(out RaycastHit hit, Vector3 __direction);
}
