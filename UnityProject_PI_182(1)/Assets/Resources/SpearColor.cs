﻿using Photon.Pun;
using UnityEngine;

public class SpearColor : MonoBehaviourPun
{

    [SerializeField] private Material _materialEnemySpear;
    [SerializeField] private MeshRenderer _meshRenderer;

    void Start()
    {
        if (!photonView.IsMine) _meshRenderer.material = _materialEnemySpear;
    }

}
