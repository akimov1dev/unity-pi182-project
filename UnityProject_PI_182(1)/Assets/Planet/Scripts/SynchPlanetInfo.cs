﻿using Photon.Pun;
using UnityEngine;

public class SynchPlanetInfo : MonoBehaviourPun, IPunObservable
{

    private GameObject[] _planets;
    private PlanetInfo[] _planetsInfo;

    void Start()
    {
        _planets = GameObject.FindGameObjectsWithTag("Planet");
        _planetsInfo = new PlanetInfo[_planets.Length];
        for (int i = 0; i < _planets.Length; i++)
        {
            _planetsInfo[i] = _planets[i].GetComponent<PlanetInfo>();
        }
    }


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {

            for (int i = 0; i < _planetsInfo.Length; i++)
            {
                stream.SendNext(_planetsInfo[i].IsBusy);
            }
        }
        else
        {

            for (int i = 0; i < _planetsInfo.Length; i++)
            {
                _planetsInfo[i].IsBusy = (bool)stream.ReceiveNext();
            }
        }
    }

}
