﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Planet
{
    [RequireComponent(typeof(PlanetRotationRangesController))]
    [RequireComponent(typeof(PlanetArrowReceiver))]
    public class PlanetPlayerReceiver : MonoBehaviour
    {
        [SerializeField] private float _minTeleportRangeSize;
        private bool _planetIsFree;

        private PlanetRotationRangesController _planetRotationRangesController;

        private void Start()
        {
            gameObject.TryGetComponent(out _planetRotationRangesController);
        }
        /// <summary>
        /// Обращаемся сюда чтобы понять можем ли мы телепортироваться на диапазон, в который попали телепортом.
        /// </summary>
        /// <param name="teleportPoint"></param>
        /// <returns></returns>
        public bool IsTeleportAllowed(RaycastHit teleportPoint) { 

            if (!_planetIsFree) return false;
            else if(GetRangeSize(teleportPoint) > _minTeleportRangeSize)
            {
                return true;
            }
            return false;
         
        }

        private float GetRangeSize(RaycastHit teleportPoint)
        {
            return _planetRotationRangesController.GetRangeSize(teleportPoint.transform.position);
        }

    }
}