﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using System.Linq;

namespace Planet
{
    [RequireComponent(typeof(PlanetArrowReceiver))]
    [RequireComponent(typeof(PlanetPlayerReceiver))]
    public class PlanetRotationRangesController : MonoBehaviour
    {
        //private List<(float, float)> _rotationRanges;
        //public List<(float, float)> RotationRanges { private get => _rotationRanges; set => _rotationRanges = value; }

        private (float, float) _currentRotationRange;
        private List<Vector3> arrowsDirectionAngle;
        private List<float> arrowsRotationAngle;
        public void AddArrowDirectionVector(Vector3 arrowDirectionVector)
        {
            arrowsDirectionAngle.Add(arrowDirectionVector);
            CalculateRanges();
        }

        private void CalculateRanges()
        {
            foreach (Vector3 arrowRotation in arrowsDirectionAngle)
            {
                arrowsRotationAngle.Add(Vector3.SignedAngle(arrowRotation, transform.right, Vector3.up));//получаем угол стрелы по Y относительно центра планеты 
            }
            arrowsRotationAngle.Sort();
        }

        public float GetRangeSize(Vector3 teleportPoint)
        {
            float rotation = Vector3.SignedAngle(transform.position - teleportPoint, transform.right, Vector3.up);
            
            return GetRotationRange(rotation).Item2- GetRotationRange(rotation).Item1;
        }
        /// <summary>
        /// Возвращает текущий диапазон ротации.
        /// </summary>
        /// <returns></returns>
        public (float, float) GetRotationRange()
        {
            return _currentRotationRange;
        }
        /// <summary>
        /// Возвращает диапазон ротации в который попадает rotation.
        /// </summary>
        /// <param name="rotation"></param>
        /// <returns></returns>
        private (float,float) GetRotationRange(float rotation)
        {
            arrowsRotationAngle.Add(rotation);
            arrowsRotationAngle.Sort();
            int rotationValueIndex = arrowsRotationAngle.IndexOf(rotation);
            if (rotationValueIndex == 0)//если индекс-1 выходит за пределы
                _currentRotationRange.Item1 = arrowsRotationAngle.Last();
                                                                                                        //_currentRotationRange.Item1 = -181;//TODO присобачить проверку на такой угол, если это он, то можно двигаться влево
            else
            {
                _currentRotationRange.Item1 = arrowsRotationAngle[rotationValueIndex - 1];
            }
            if(arrowsRotationAngle.Count == rotationValueIndex+1)//если индекс+1 выходит за пределы
                _currentRotationRange.Item2 = arrowsRotationAngle[0];
                                                                                                        //_currentRotationRange.Item2 = 181;//TODO присобачить проверку на такой угол, если это он, то можно двигаться вправо
            else
            {
                _currentRotationRange.Item2 = arrowsRotationAngle[rotationValueIndex + 1];
            }
            arrowsRotationAngle.Remove(rotation);
            return _currentRotationRange;
        }
        
        #region Useless but sorry to delete
        //private void Update()
        //{
        //    var rotation = transform.rotation;
        //    y = Clamp(rotation.eulerAngles.y, 0, -6);

        //    rotation = Quaternion.Euler(0, y, 0);
        //    //Mathf.Clamp(rotation.eulerAngles.y, -50, 0);
        //    transform.rotation = rotation;
        //}

        //private float Clamp(float value, float limit1, float limit2)
        //{
        //    float min = limit1 < limit2 ? limit1 : limit2;
        //    float max = limit1 > limit2 ? limit1 : limit2;
        //    if (value < min)
        //    {
        //        return min;
        //    }
        //    else if(value>max)
        //    {
        //        return max;
        //    }
        //    return value;
        //}
        #endregion
    }
}