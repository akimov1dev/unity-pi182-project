﻿using Photon.Pun;
using UnityEngine;

public class PlanetInfo : MonoBehaviourPun
{
    public bool IsBusy { get; set; }


    public void SetIsBusyValue(bool value)
    {
        IsBusy = value;
        photonView.RPC("SynchPlanetInfo", RpcTarget.OthersBuffered, value);
    }

    [PunRPC]
    private void SynchPlanetInfo(bool value)
    {
        IsBusy = value;
    }

}


