﻿using Planet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Planet
{
    [RequireComponent(typeof(PlanetRotationRangesController))]
    [RequireComponent(typeof(PlanetPlayerReceiver))]
    
    public class PlanetArrowReceiver : MonoBehaviour
    {
        private PlanetRotationRangesController _planetRotationRangesController;

        private void Start()
        {
            gameObject.TryGetComponent(out _planetRotationRangesController);
        }
        /// <summary>
        /// Мы ДОЛЖНЫ "установить" стрелу чтобы планета обработала ее.
        /// </summary>
        /// <param name="arrow">Стрела, которую мы заспавнили на планете.</param>
        public void SetArrow(Arrow arrow)
        {
            InformAboutArrow(arrow.transform.forward);
        }

        private void InformAboutArrow(Vector3 arrowDirection) {

            _planetRotationRangesController.AddArrowDirectionVector(arrowDirection);

        }
    }
}
