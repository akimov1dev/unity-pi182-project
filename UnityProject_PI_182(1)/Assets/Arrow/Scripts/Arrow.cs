﻿using System.Collections;
using UnityEngine;
using Photon.Pun;

public class Arrow : MonoBehaviourPun
{

    [SerializeField] private float _speed = 15f;
    [SerializeField] private float _timeLive = 3f;
    [SerializeField] private int _damage = 1;
    [SerializeField] private int _pointCount = 1;
    [SerializeField] private int _pointCountKill = 5;
    [SerializeField] private GameObject _newSpear;

    private bool _isMove = true;
    RaycastHit _raycastHit = new RaycastHit();

    private PlayerPoints _playerPoints;

    private void Start()
    {
        StartCoroutine(DestroySpear());
    }

    private void Update()
    {

        if (_isMove)
        {
            Ray ray = new Ray(transform.position, transform.forward);
            _isMove = !Physics.Raycast(ray, out _raycastHit, 2f + _speed * Time.deltaTime);
            transform.Translate(transform.forward * _speed * Time.deltaTime, Space.World);
        }
        else
        {
            Health health = _raycastHit.collider.gameObject.GetComponent<Health>();
            if (health != null) 
            {
                health.TakeDamage(_damage);
                if (photonView.IsMine)
                {
                    _playerPoints.AddPointCount(_pointCount);
                    if (health.CurrentHealth == 1) _playerPoints.AddPointCount(_pointCountKill);
                }

            }
            GameObject newSpear = Instantiate(_newSpear, transform.position, transform.rotation);
            newSpear.GetComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().material;
            newSpear.transform.SetParent(_raycastHit.transform);
            Destroy(gameObject);
        }
    }

    IEnumerator DestroySpear()
    {
        yield return new WaitForSeconds(_timeLive);
        Destroy(gameObject);
    }

    public void SetPlayer(PlayerPoints playerPoints)
    {
        _playerPoints = playerPoints;
    }

}
