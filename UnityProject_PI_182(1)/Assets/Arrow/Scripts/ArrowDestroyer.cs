﻿using UnityEngine;

public class ArrowDestroyer : MonoBehaviour
{
    [SerializeField] private float _liveTime = 10f;

    void Start()
    {
        Destroy(gameObject, _liveTime);
    }

}
