﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviourPunCallbacks
{

    [SerializeField] TMP_Text _nickname = null;
    [SerializeField] Text _ErrorText = null;
    [SerializeField] Button _buttonPlay = null;

    private bool _isDisconnected;

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "v1.0";
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.ConnectToRegion("ru");
        _buttonPlay.interactable = false;
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to Master");
        _isDisconnected = false;
        _ErrorText.text = "";
        _buttonPlay.interactable = true;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {

        if (!_isDisconnected)
        {
            if (DisconnectCause.ExceptionOnConnect == cause) _ErrorText.text = "Нет подключения к сети";
            if (DisconnectCause.ClientTimeout == cause) _ErrorText.text = "Соединение прервано";
            _isDisconnected = true;
            _buttonPlay.interactable = false;
            StartCoroutine(ConnectingTry());
        }
        else
        {
            StartCoroutine(ConnectingTry());
        }
    }

    private IEnumerator ConnectingTry()
    {
        yield return new WaitForSeconds(5);
        _ErrorText.text = "Попытка соединения";
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.ConnectToRegion("ru");
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("Room");
    }

    public void Play()
    {

        if (_isDisconnected) return;

        if (!CheckNickname(_nickname.text, out string message))
        {
            _ErrorText.text = message;
            return;
        }
        else
        {
            _ErrorText.text = "";
        }

        PhotonNetwork.NickName = _nickname.text;

        PhotonNetwork.JoinOrCreateRoom("Room", new RoomOptions { MaxPlayers = 20 }, TypedLobby.Default);
    }

    private bool CheckNickname(string nickname, out string message)
    {
        if (nickname.Length == 1)//особенность TMPro
        {
            message = "Ник не может быть пустым";
            return false;
        }

        for (int i = 0; i < nickname.Length; i++)
        {
            if (nickname[i] == ' ')
            {
                message = "Ник не может содержать пробелы";
                return false;
            }
        }

        message = "";

        return true;
    }

}
