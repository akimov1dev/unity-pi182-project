﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviourPunCallbacks
{


    [SerializeField] private GameObject _playerPrefab = null;
    [SerializeField] private Text _pingText = null;

    [Header("Top Players")]
    //[SerializeField] private TMP_Text _nick_1;
    //[SerializeField] private TMP_Text _nick_2;
    //[SerializeField] private TMP_Text _nick_3;
    //[SerializeField] private TMP_Text _nick_4;
    //[SerializeField] private TMP_Text _nick_5;
    //[SerializeField] private TMP_Text _nick_6;
    //[SerializeField] private TMP_Text _nick_7;
    //[SerializeField] private TMP_Text _nick_8;
    //[SerializeField] private TMP_Text _nick_9;
    //[SerializeField] private TMP_Text _nick_10;
    [SerializeField] private List<TMP_Text> topPlayersNick = new List<TMP_Text>();
    private GameObject[] _players;

    void Start()
    {

        if (_playerPrefab == null)
        {
            Debug.LogError("Где префаб игрока?");
            return;
        }

        GameObject[] planets = GameObject.FindGameObjectsWithTag("Planet");
        GameObject[] freePlanets = new GameObject[planets.Length];
        int countFreePlanets = 0;

        for (int i = 0; i < planets.Length; i++)
        {
            if (!planets[i].GetComponent<PlanetInfo>().IsBusy)
            {
                freePlanets[countFreePlanets] = planets[i];
                countFreePlanets++;
            }
        }

        GameObject startPlanet = freePlanets[Random.Range(0, countFreePlanets)];
        GameObject player = PhotonNetwork.Instantiate(_playerPrefab.name, startPlanet.transform.position, Quaternion.identity);
        PlayerController playerController = player.GetComponent<PlayerController>();
        playerController.SetStartPlanet(startPlanet);
        playerController.SetColor(Random.ColorHSV());

        StartCoroutine(DisplayLeaderBoard());
    }

    private void Update()
    {
        _pingText.text = "Ping: " + PhotonNetwork.GetPing();
    }


    IEnumerator DisplayLeaderBoard()
    {
        yield return new WaitForSeconds(1f);

        _players = GameObject.FindGameObjectsWithTag("Player");
        PlayerPoints[] playersPoints = new PlayerPoints[_players.Length];
        for (int i = 0; i < _players.Length; i++)
        {
            playersPoints[i] = _players[i].GetComponent<PlayerPoints>();
        }

        for (var i = 1; i < playersPoints.Length; i++)
        {
            for (var j = 0; j < playersPoints.Length - i; j++)
            {
                if (playersPoints[j].PointCount > playersPoints[j + 1].PointCount)
                {
                    PlayerPoints playerPointsTemp = playersPoints[j];
                    playersPoints[j] = playersPoints[j + 1];
                    playersPoints[j + 1] = playerPointsTemp;
                }
            }
        }

        //for (int i = 0; i < 10; i++)
        //{
        //    topPlayersNick[i].text = "";
        //}
        foreach(TMP_Text nick in topPlayersNick)
        {
            nick.text = "";
        }

        //_nick_1.text = topPlayersNick[0].text;
        //_nick_2.text = topPlayersNick[1].text;
        //_nick_3.text = topPlayersNick[2].text;
        //_nick_4.text = topPlayersNick[3].text;
        //_nick_5.text = topPlayersNick[4].text;
        //_nick_6.text = topPlayersNick[5].text;
        //_nick_7.text = topPlayersNick[6].text;
        //_nick_8.text = topPlayersNick[7].text;
        //_nick_9.text = topPlayersNick[8].text;
        //_nick_10.text = topPlayersNick[9].text;
        for (int i = 0, j = playersPoints.Length - 1; i < playersPoints.Length; i++, j--)
        {
            topPlayersNick[i].text = playersPoints[j].NickName + "  " + playersPoints[j].PointCount.ToString();
            if (i == 9) break;
        }

        StartCoroutine(DisplayLeaderBoard());
    }


    public void Leave()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel("Lobby");
    }


    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
    }
}
