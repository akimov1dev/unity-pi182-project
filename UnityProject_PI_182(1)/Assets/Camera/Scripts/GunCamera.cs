﻿using UnityEngine;

public class GunCamera : MonoBehaviour
{
    public Transform PlayerTransform { get; set; }
    [SerializeField] private float TransitionTime = 1.0f;
    [SerializeField] private float GunDistance = 10.0f;

    private Transform _transform;
    private Vector3 _transitionVelocity = Vector3.zero;

    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    void Update()
    {

        if (PlayerTransform == null) return;

        // Плавное смещение камеры к позиции игрока.
        _transform.position = Vector3.SmoothDamp(_transform.position, new Vector3(PlayerTransform.position.x, GunDistance, PlayerTransform.position.z), ref _transitionVelocity, TransitionTime);

        // Изменение дистанции камеры до игрока.
        GunDistance += Input.GetAxis("Mouse ScrollWheel") * -15.0f;

        GunDistance = Mathf.Clamp(GunDistance, 50, 80);
    }
}
