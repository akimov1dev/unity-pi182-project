﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class Gun : MonoBehaviour, IGun
{
    private GameObject _spear;
    private Transform _player;
    private float _firerate = 3f;
    private float _nextTimeToFire = 0.0f;
    Arrow _arrow = new Arrow();



    public Gun(GameObject spear, Transform playerTransform)
    {
        _spear = spear;
        _player = playerTransform;
    }

    public void Shoot()
    {

        if (_spear != null && Time.time > _nextTimeToFire)
      {
        _nextTimeToFire = Time.time + 1.0f / _firerate;

        Ray ray = new Ray(_player.position, _player.right);
        RaycastHit hit;


            if (Physics.Raycast(ray, out hit))
            {
       
               GameObject spear = Instantiate(_spear);
               spear.transform.position = hit.point;
               spear.transform.SetParent(hit.transform);
               _arrow.ArrowRotation(ray, hit, spear, _player);

                if (hit.transform.tag != "Planet")
                {
                    Health health = hit.transform.GetComponent<Health>();
                    if (health != null)
                    {
                        health.TakeDamage(1);
                    }
                }
              

            }
	  }
    }



    public bool GetPlanetToTeleport(out RaycastHit hit, Vector3 __direction)
    {

        Ray ray = new Ray(transform.position, __direction);
        if (Physics.Raycast(ray, out hit) && hit.transform.TryGetComponent(out Object planet))
        {
            return true;
        }
        return false;
    }

}