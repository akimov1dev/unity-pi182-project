﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float health = 5f;
    public void TakeDamage(float amount)
    {
        health -= amount;
        if(health <= 0f)
        {

            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
