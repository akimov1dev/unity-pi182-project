﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{


    public void ArrowRotation(Ray ray, RaycastHit _hit, GameObject _spear, Transform _player)
    {
        float angle = Vector3.SignedAngle(Vector3.right, _hit.normal, Vector3.up);
        _spear.transform.Rotate(0.0f, 90.0f, angle);
    }


}
