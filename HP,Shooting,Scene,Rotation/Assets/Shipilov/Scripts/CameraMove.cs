﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    public Transform anchor;

    void LateUpdate()
    {
        Vector3 newPos = new Vector3(anchor.position.x, transform.position.y, anchor.position.z);
        transform.position = newPos;
    }
}
