﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    public GameObject planet;
    float speed = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.W))
        {
            planet.transform.Rotate(Vector3.forward * 180f);
            transform.Rotate(Vector3.forward * 180f);
            planet.transform.Translate(planet.transform.forward * speed * Time.deltaTime);
            transform.Translate(transform.forward * speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S))
        {

            planet.transform.Rotate(Vector3.forward * 180f);
            transform.Rotate(Vector3.forward * 180f);
            planet.transform.Translate(-planet.transform.forward * speed * Time.deltaTime);
            transform.Translate(-transform.forward * speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.Q))
        {

            planet.transform.Rotate(Vector3.forward * 180f);
            transform.Rotate(Vector3.forward * 180f);
            planet.transform.Translate(planet.transform.forward * speed * Time.deltaTime);
            transform.Translate(transform.forward * speed * Time.deltaTime);
            planet.transform.Translate(-planet.transform.right * speed * Time.deltaTime);
            transform.Translate(-transform.right * speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.E))
        {

            planet.transform.Rotate(Vector3.forward * 180f);
            transform.Rotate(Vector3.forward * 180f);
            planet.transform.Translate(planet.transform.forward * speed * Time.deltaTime);
            transform.Translate(transform.forward * speed * Time.deltaTime);
            planet.transform.Translate(planet.transform.right * speed * Time.deltaTime);
            transform.Translate(transform.right * speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            planet.transform.Rotate(Vector3.right * 180f);
            transform.Rotate(Vector3.right * 180f);
            planet.transform.Translate(-planet.transform.right * speed * Time.deltaTime);
            transform.Translate(-transform.right * speed * Time.deltaTime);
        }

        else if (Input.GetKey(KeyCode.D))
        {
            planet.transform.Rotate(Vector3.right * 180f);
            transform.Rotate(Vector3.right * 180f);
            planet.transform.Translate(planet.transform.right * speed * Time.deltaTime);
            transform.Translate(transform.right * speed * Time.deltaTime);
        }
    }
}
