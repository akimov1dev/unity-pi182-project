﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Rotation : MonoBehaviour
{
    private bool facingright = true;
    public Camera mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pointer = new Vector3(Input.mousePosition.x, Input.mousePosition.y, mainCamera.nearClipPlane);
        Vector3 worldpos = mainCamera.ScreenToWorldPoint(pointer);
        Vector3 for_rotation = new Vector3(worldpos.x, 0.0f, worldpos.z);
        transform.rotation = Quaternion.LookRotation(for_rotation);

        if (facingright == true && transform.rotation.z > 0)
        {
            Flip();
        }
        else if (facingright == true && transform.rotation.z < 0)
        {
            Flip();
        }

    void Flip()
        {
            facingright = !facingright;
            transform.Rotate(0f, 180f, 0f);
        }

    }
}
