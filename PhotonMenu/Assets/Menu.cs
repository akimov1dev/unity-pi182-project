﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{

    public TMP_Text Nickname;

    public void AddEmodji(TMP_Text emodji)
    {
        if (Nickname != null & Nickname.text.Length > 1)
        {
            Nickname.spriteAsset = emodji.spriteAsset;
            Nickname.text += emodji.text;
            print(emodji.text);
        }
    }
}
