﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net.Http;
using UnityEngine.UIElements;

public static class Serializer 
{

    public static byte[][] SerializeTransforms(List<Transform> transforms)
    {
        List<byte[]> serializedTransformsByteArray = new List<byte[]>();
        foreach (Transform transform in transforms)
        {
            SerializableTransform serializableTransform = new SerializableTransform();
            serializableTransform.position = transform.position;
            serializableTransform.rotation = transform.rotation;
            serializedTransformsByteArray.Add(ObjectToByteArray(serializableTransform));
        }

        return serializedTransformsByteArray.ToArray();
    }

    public static List<SerializableTransform> DeserializeTransformsByteArray(byte[][] byteArrays)//мы не можем динамично создавать трансформы
    {
        List<SerializableTransform> serializedTransforms = new List<SerializableTransform>();
        foreach (byte[] byteArray in byteArrays)
        {
            serializedTransforms.Add((SerializableTransform)ByteArrayToObject(byteArray));
        }
        return serializedTransforms;
    }

    private static byte[] ObjectToByteArray(Object obj)
    {
        if (obj == null)
            return null;

        BinaryFormatter bf = new BinaryFormatter();
        MemoryStream ms = new MemoryStream();
        bf.Serialize(ms, obj);

        return ms.ToArray();
    }

    private static Object ByteArrayToObject(byte[] arrBytes)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();
        memStream.Write(arrBytes, 0, arrBytes.Length);
        memStream.Seek(0, SeekOrigin.Begin);
        Object obj = (Object)binForm.Deserialize(memStream);

        return obj;
    }

}
public class SerializableTransform: MonoBehaviour
{
    public Vector3 position;
    public Quaternion rotation;
}