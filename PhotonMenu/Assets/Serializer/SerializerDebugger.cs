﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerializerDebugger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        List<Transform> transforms = new List<Transform>();
        transforms.Add(transform);
        byte[][] temp = Serializer.SerializeTransforms(transforms);
        float x = Serializer.DeserializeTransformsByteArray(temp)[0].position.x;
        float y = Serializer.DeserializeTransformsByteArray(temp)[0].position.y;
        float z = Serializer.DeserializeTransformsByteArray(temp)[0].position.z;
        foreach (byte[] transform in temp)
        Debug.Log(x+" " + y + " " +z);
    }

}
